terraform {
  backend "s3" {
    bucket = "my-epam-diploma-bucket"
    key    = "tf-states/eks-state.tfstate"
    region = "eu-north-1"
  }
}

provider "aws" {
  region = var.region
}