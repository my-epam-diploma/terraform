variable "region" {
  default     = "eu-north-1"
  description = "AWS region"
}

variable "gitlab_agent_token" {
  default     = ""
  description = "Gitlab Agent token"
}
