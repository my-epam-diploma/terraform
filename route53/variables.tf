variable "region" {
  default     = "eu-north-1"
  description = "AWS region"
}

variable "zoneid" {
  default = "Z0563275AP9ZSVTU6L6I"
  description = "Route53 zone id for epam-diploma.tk" 
}
