# Terraform
WIP
IaaC EKS cluster in AWS and installation of Gitlab-agent

# Usefull commands

## Configure local kubeconfig to connect to EKS with kubectl
```bash
aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)
```

## Deploy Kubernetes Metrics Server
```bash
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.1/components.yaml
```

## Install gitlab agent with Helm
```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set config.token=rNVFuGNKQnbzwisZK8M4QG7hEFyuWbJ_AUoubr14bdXzFAAPvQ \
    --set config.kasAddress=wss://kas.gitlab.com
```

## Run terraform in different dir
```bash
terraform -chdir=./gitlab-agent/ init
terraform -chdir=./gitlab-agent/ apply
```
## Check VPC CNI add-on
```bash
aws eks describe-addon \
    --cluster-name diploma-eks-GtjbE0FA \
    --addon-name vpc-cni \
    --query "addon.addonVersion" \
    --output text
```
```bash
aws eks update-addon \
    --cluster-name diploma-eks-T4SQcly6 \
    --addon-name vpc-cni \
    --addon-version v1.11.0-eksbuild.1 \
    --resolve-conflicts OVERWRITE
```
## Apply Terraform but change VARIABLE
```bash
terraform apply -var "my_var=some_var"
```

## Update variable in gitlab
```bash
curl --request PUT --header "PRIVATE-TOKEN: API_TOKEN" \
     "https://gitlab.com/api/v4/projects/35719300/variables/KUBECONFIG" \
     --form "value=$(terraform output -raw kubeconfig)"
```

## Install kubectl
```bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.6/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
```