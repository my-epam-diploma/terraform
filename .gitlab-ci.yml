image:
  name: alpine:3.15

stages:
  - terraform-eks
  - kubectl
  - terraform-nodegroups
  - terraform-helm
  - terraform-rds
  - terraform-route53
  - destroy-everything

terraform-eks:  
  stage: terraform-eks
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git curl --no-cache
  script:
  - terraform -chdir=./eks/ init
  - terraform -chdir=./eks/ plan
  - terraform -chdir=./eks/ apply --auto-approve
  - |
    curl --request PUT --header "PRIVATE-TOKEN: $API_TOKEN" \
    "https://gitlab.com/api/v4/projects/35719300/variables/KUBECONFIG" \
    --form "value=$(terraform -chdir=./eks/ output -raw kubeconfig)"
  when: manual

kubectl:
  stage: kubectl
  needs: ["terraform-eks"]
  before_script:
  - apk add curl --no-cache
  - curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.6/bin/linux/amd64/kubectl
  - chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl
  - curl -o aws-iam-authenticator https://s3.us-west-2.amazonaws.com/amazon-eks/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator
  - chmod +x ./aws-iam-authenticator && mv ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator
  - export KUBECONFIG="${KUBECONFIG}"
  script:
  - kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/aws/deploy.yaml
  - kubectl apply -f eks-admin.yaml
  - kubectl set env daemonset aws-node -n kube-system ENABLE_PREFIX_DELEGATION=true
  when: on_success

terraform-nodegroups:
  stage: terraform-nodegroups
  needs: ["kubectl"]
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git --no-cache
  script:
  - terraform -chdir=./nodegroups/ init
  - terraform -chdir=./nodegroups/ plan
  - terraform -chdir=./nodegroups/ apply --auto-approve
  when: on_success

terraform-helm:
  stage: terraform-helm
  image:
    name: ubuntu:20.04
  needs: ["terraform-nodegroups"]
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apt update && apt install curl unzip -y
  - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  - unzip awscliv2.zip
  - ./aws/install
  - curl "https://releases.hashicorp.com/terraform/1.1.9/terraform_1.1.9_linux_amd64.zip" -o "terraform.zip"
  - unzip terraform.zip && chmod +x ./terraform && mv ./terraform /usr/local/bin/terraform
  script:
  - terraform -chdir=./helm/ init
  - terraform -chdir=./helm/ plan
  - terraform -chdir=./helm/ apply -var gitlab_agent_token="${AGENT_TOKEN}" --auto-approve
  when: on_success

terraform-route53:
  stage: terraform-route53
  needs: ["kubectl"]
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git --no-cache
  script:
  - terraform -chdir=./route53/ init
  - terraform -chdir=./route53/ plan
  - terraform -chdir=./route53/ apply --auto-approve
  when: on_success

terraform-rds:
  stage: terraform-rds
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git curl --no-cache
  script:
  - terraform -chdir=./rds/ init
  - terraform -chdir=./rds/ plan
  - terraform -chdir=./rds/ apply -var db_name="${DB_NAME}" -var db_username="${DB_USERNAME}" -var db_password="${DB_PASSWORD}" --auto-approve
  - |
    curl --request PUT --header "PRIVATE-TOKEN: $API_TOKEN" \
    "https://gitlab.com/api/v4/groups/16616501/variables/RDS_ENDPOINT" \
    --form "value=$(terraform -chdir=./rds/ output -raw rds_endpoint | grep -Eo '\S+.com')"
  when: manual

0-destroy-route53:
  stage: destroy-everything
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git --no-cache
  script:
  - terraform -chdir=./route53/ init
  - terraform -chdir=./route53/ destroy --auto-approve
  when: manual

1-destroy-ingress:
  stage: destroy-everything
  before_script:
  - apk add curl --no-cache
  - curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.6/bin/linux/amd64/kubectl
  - chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl
  - curl -o aws-iam-authenticator https://s3.us-west-2.amazonaws.com/amazon-eks/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator
  - chmod +x ./aws-iam-authenticator && mv ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator
  - export KUBECONFIG="${KUBECONFIG}"
  script:
  - kubectl delete -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/aws/deploy.yaml
  allow_failure: true
  when: manual

2-destroy-nodegroups:
  stage: destroy-everything
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git --no-cache
  script:
  - terraform -chdir=./nodegroups/ init
  - terraform -chdir=./nodegroups/ destroy --auto-approve
  when: manual

3-destroy-rds:
  stage: destroy-everything
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git --no-cache
  script:
  - terraform -chdir=./rds/ init
  - terraform -chdir=./rds/ destroy --auto-approve
  when: manual

4-destroy-eks:
  stage: destroy-everything
  before_script:
  - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
  - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
  - apk add terraform git --no-cache
  script:
  - terraform -chdir=./eks/ init
  - terraform -chdir=./eks/ destroy --auto-approve
  when: manual
