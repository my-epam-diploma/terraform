terraform {
  backend "s3" {
    bucket = "my-epam-diploma-bucket"
    key    = "tf-states/rds-state.tfstate"
    region = "eu-north-1"
  }
}

provider "aws" {
  region = var.region
}

data "terraform_remote_state" "eks" {
  backend = "s3"
  config = {
    bucket = "my-epam-diploma-bucket"
    key = "tf-states/eks-state.tfstate"
    region = "eu-north-1"
  }
}