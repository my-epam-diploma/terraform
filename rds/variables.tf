variable "region" {
  default     = "eu-north-1"
  description = "AWS region"
}

variable "db_name" {
  default     = ""
}
variable "db_username" {
  default     = ""
}
variable "db_password" {
  default     = ""
}