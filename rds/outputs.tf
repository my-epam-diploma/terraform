# RDS endpoint
output "rds_endpoint" {
  value = aws_db_instance.rds.endpoint
  sensitive = true
}