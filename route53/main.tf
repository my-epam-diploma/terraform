terraform {
  backend "s3" {
    bucket = "my-epam-diploma-bucket"
    key    = "tf-states/route53-state.tfstate"
    region = "eu-north-1"
  }
}

provider "aws" {
  region = var.region
}

data "aws_lb" "ingress" {
}

resource "aws_route53_record" "prod" {
  zone_id = var.zoneid
  name    = "prod.epam-diploma.tk"
  type    = "A"

  alias {
    name                   = data.aws_lb.ingress.dns_name
    zone_id                = data.aws_lb.ingress.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "staging" {
  zone_id = "Z0563275AP9ZSVTU6L6I"
  name    = "staging.epam-diploma.tk"
  type    = "A"

  alias {
    name                   = data.aws_lb.ingress.dns_name
    zone_id                = data.aws_lb.ingress.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "sonarqube" {
  zone_id = "Z0563275AP9ZSVTU6L6I"
  name    = "sonarqube.epam-diploma.tk"
  type    = "A"

  alias {
    name                   = data.aws_lb.ingress.dns_name
    zone_id                = data.aws_lb.ingress.zone_id
    evaluate_target_health = true
  }
}